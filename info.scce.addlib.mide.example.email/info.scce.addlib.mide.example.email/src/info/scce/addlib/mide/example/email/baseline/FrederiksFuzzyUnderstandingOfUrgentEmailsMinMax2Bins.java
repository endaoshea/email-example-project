/* This file was generated with the ADD-Lib
 * http://add-lib.scce.info/ */

package info.scce.addlib.mide.example.email.baseline;

import info.scce.addlib.mide.example.email.EmailPredicates;

public class FrederiksFuzzyUnderstandingOfUrgentEmailsMinMax2Bins {

	public double emailUrgent(EmailPredicates predicates) {
		return eval140710878609792(predicates);
	}

	private double eval140710878609792(EmailPredicates predicates) {
		if (predicates.subjectContainsUrgent())
			return eval140710878609728(predicates);
		else
			return eval140710878609760(predicates);
	}

	private double eval140710878609760(EmailPredicates predicates) {
		if (predicates.subjectContainsDeadline())
			return eval140710878609728(predicates);
		else
			return eval140710878608064(predicates);
	}

	private double eval140710878609728(EmailPredicates predicates) {
		if (predicates.subjectContainsNewsletter())
			return eval140710878609632(predicates);
		else
			return eval140710878609696(predicates);
	}

	private double eval140710878609696(EmailPredicates predicates) {
		if (predicates.fromGov())
			return eval140710878609600(predicates);
		else
			return eval140710878609664(predicates);
	}

	private double eval140710878609664(EmailPredicates predicates) {
		if (predicates.inAddressBook())
			return eval140710878609600(predicates);
		else
			return eval140710878608064(predicates);
	}

	private double eval140710878609600(EmailPredicates predicates) {
		if (predicates.fromShopA())
			return eval140710878608064(predicates);
		else
			return eval140710878609568(predicates);
	}

	private double eval140710878609568(EmailPredicates predicates) {
		if (predicates.fromShopB())
			return eval140710878608064(predicates);
		else
			return eval140710878607072(predicates);
	}

	private double eval140710878609632(EmailPredicates predicates) {
		if (predicates.fromGov())
			return eval140710878609600(predicates);
		else
			return eval140710878608064(predicates);
	}

	private double eval140710878608064(EmailPredicates predicates) {
		return 0.0;
	}

	private double eval140710878607072(EmailPredicates predicates) {
		return 1.0;
	}
}
