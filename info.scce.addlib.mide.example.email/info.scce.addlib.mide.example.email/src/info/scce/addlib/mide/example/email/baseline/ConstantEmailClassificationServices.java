/* This file was generated with the ADD-Lib
 * http://add-lib.scce.info/ */

package info.scce.addlib.mide.example.email.baseline;

import info.scce.addlib.mide.example.email.EmailPredicates;

public class ConstantEmailClassificationServices {

    public boolean alwaysYes(EmailPredicates predicates) {
        return eval140433556647968(predicates);
    }

    public boolean alwaysNo(EmailPredicates predicates) {
        return eval140433556648096(predicates);
    }

    private boolean eval140433556647968(EmailPredicates predicates) {
        return true;
    }

    private boolean eval140433556648096(EmailPredicates predicates) {
        return false;
    }
}
