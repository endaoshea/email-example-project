/* This file was generated with the ADD-Lib
 * http://add-lib.scce.info/ */

package info.scce.addlib.mide.example.email.baseline;

import info.scce.addlib.mide.example.email.EmailPredicates;

public class FrederiksFuzzyUnderstandingOfUrgentEmailsMinMax {

    public double emailUrgent(EmailPredicates predicates) {
        return eval140711013417600(predicates);
    }

    private double eval140711013417600(EmailPredicates predicates) {
        if (predicates.subjectContainsUrgent())
            return eval140711013417216(predicates);
        else
            return eval140711013417568(predicates);
    }

    private double eval140711013417568(EmailPredicates predicates) {
        if (predicates.subjectContainsDeadline())
            return eval140711013417344(predicates);
        else
            return eval140711013417536(predicates);
    }

    private double eval140711013417536(EmailPredicates predicates) {
        if (predicates.subjectContainsNewsletter())
            return eval140711013417440(predicates);
        else
            return eval140711013417504(predicates);
    }

    private double eval140711013417504(EmailPredicates predicates) {
        if (predicates.fromGov())
            return eval140711013417408(predicates);
        else
            return eval140711013417472(predicates);
    }

    private double eval140711013417472(EmailPredicates predicates) {
        if (predicates.inAddressBook())
            return eval140711013417408(predicates);
        else
            return eval140711013416128(predicates);
    }

    private double eval140711013417408(EmailPredicates predicates) {
        if (predicates.fromShopA())
            return eval140711013416704(predicates);
        else
            return eval140711013417376(predicates);
    }

    private double eval140711013417376(EmailPredicates predicates) {
        if (predicates.fromShopB())
            return eval140711013416704(predicates);
        else
            return eval140711013415360(predicates);
    }

    private double eval140711013417440(EmailPredicates predicates) {
        if (predicates.fromGov())
            return eval140711013417408(predicates);
        else
            return eval140711013417056(predicates);
    }

    private double eval140711013417056(EmailPredicates predicates) {
        if (predicates.inAddressBook())
            return eval140711013417024(predicates);
        else
            return eval140711013416128(predicates);
    }

    private double eval140711013417024(EmailPredicates predicates) {
        if (predicates.fromShopA())
            return eval140711013416704(predicates);
        else
            return eval140711013416992(predicates);
    }

    private double eval140711013416992(EmailPredicates predicates) {
        if (predicates.fromShopB())
            return eval140711013416704(predicates);
        else
            return eval140711013415712(predicates);
    }

    private double eval140711013417344(EmailPredicates predicates) {
        if (predicates.subjectContainsOverdue())
            return eval140711013417216(predicates);
        else
            return eval140711013417312(predicates);
    }

    private double eval140711013417312(EmailPredicates predicates) {
        if (predicates.subjectContainsNewsletter())
            return eval140711013417088(predicates);
        else
            return eval140711013417280(predicates);
    }

    private double eval140711013417280(EmailPredicates predicates) {
        if (predicates.fromGov())
            return eval140711013416960(predicates);
        else
            return eval140711013417248(predicates);
    }

    private double eval140711013417248(EmailPredicates predicates) {
        if (predicates.inAddressBook())
            return eval140711013416960(predicates);
        else
            return eval140711013416128(predicates);
    }

    private double eval140711013416960(EmailPredicates predicates) {
        if (predicates.fromShopA())
            return eval140711013416704(predicates);
        else
            return eval140711013416928(predicates);
    }

    private double eval140711013416928(EmailPredicates predicates) {
        if (predicates.fromShopB())
            return eval140711013416704(predicates);
        else
            return eval140711013415296(predicates);
    }

    private double eval140711013417088(EmailPredicates predicates) {
        if (predicates.fromGov())
            return eval140711013416960(predicates);
        else
            return eval140711013417056(predicates);
    }

    private double eval140711013417216(EmailPredicates predicates) {
        if (predicates.subjectContainsNewsletter())
            return eval140711013417088(predicates);
        else
            return eval140711013417184(predicates);
    }

    private double eval140711013417184(EmailPredicates predicates) {
        if (predicates.fromGov())
            return eval140711013417152(predicates);
        else
            return eval140711013416864(predicates);
    }

    private double eval140711013416864(EmailPredicates predicates) {
        if (predicates.inAddressBook())
            return eval140711013416832(predicates);
        else
            return eval140711013416128(predicates);
    }

    private double eval140711013416832(EmailPredicates predicates) {
        if (predicates.fromShopA())
            return eval140711013416704(predicates);
        else
            return eval140711013416800(predicates);
    }

    private double eval140711013416800(EmailPredicates predicates) {
        if (predicates.fromShopB())
            return eval140711013416704(predicates);
        else
            return eval140711013416096(predicates);
    }

    private double eval140711013417152(EmailPredicates predicates) {
        if (predicates.fromShopA())
            return eval140711013416704(predicates);
        else
            return eval140711013417120(predicates);
    }

    private double eval140711013417120(EmailPredicates predicates) {
        if (predicates.fromShopB())
            return eval140711013416704(predicates);
        else
            return eval140711013415776(predicates);
    }

    private double eval140711013416128(EmailPredicates predicates) {
        return 0.0;
    }

    private double eval140711013415360(EmailPredicates predicates) {
        return 0.2;
    }

    private double eval140711013416704(EmailPredicates predicates) {
        return 0.09999999999999998;
    }

    private double eval140711013415712(EmailPredicates predicates) {
        return 0.15000000000000002;
    }

    private double eval140711013415296(EmailPredicates predicates) {
        return 0.6;
    }

    private double eval140711013416096(EmailPredicates predicates) {
        return 0.7;
    }

    private double eval140711013415776(EmailPredicates predicates) {
        return 0.8;
    }
}
