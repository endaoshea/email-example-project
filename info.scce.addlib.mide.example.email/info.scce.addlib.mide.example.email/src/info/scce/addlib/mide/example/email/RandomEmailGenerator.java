package info.scce.addlib.mide.example.email;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomEmailGenerator {

	private final Random rand = new Random();

	public Email next() {
		return new Email(rand.nextBoolean(), rand.nextBoolean(), rand.nextBoolean(), rand.nextBoolean(),
				rand.nextBoolean(), rand.nextBoolean(), rand.nextBoolean(), rand.nextBoolean());
	}

	public List<Email> next(int n) {
		List<Email> nextN = new ArrayList<>();
		for (int i = 0; i < n; i++)
			nextN.add(next());
		return nextN;
	}
}
