package info.scce.addlib.mide.example.email;

import java.util.List;

public class SimulationThread extends Thread {

	private static final int DELAY_IN_MS = 10;

	private final List<ServiceAdapter<?>> services;
	private final RandomEmailGenerator rand = new RandomEmailGenerator();

	public SimulationThread(List<ServiceAdapter<?>> services) {
		this.services = services;
	}

	@Override
	public void run() {
		try {
			while (!isInterrupted()) {

				/* Generate random email */
				Email email = rand.next();
				System.out.println(email);

				/* Use services */
				for (ServiceAdapter<?> s : services)
					s.post(email);

				/* Wait a bit */
				Thread.sleep(DELAY_IN_MS);
			}
		} catch (InterruptedException e) {
		}
	}
}
