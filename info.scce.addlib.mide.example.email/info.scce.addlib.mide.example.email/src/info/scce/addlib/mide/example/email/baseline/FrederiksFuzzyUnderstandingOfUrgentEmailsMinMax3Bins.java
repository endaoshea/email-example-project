/* This file was generated with the ADD-Lib
 * http://add-lib.scce.info/ */

package info.scce.addlib.mide.example.email.baseline;

import info.scce.addlib.mide.example.email.EmailPredicates;

public class FrederiksFuzzyUnderstandingOfUrgentEmailsMinMax3Bins {

    public double emailUrgent(EmailPredicates predicates) {
        return eval140710993280096(predicates);
    }

    private double eval140710993280096(EmailPredicates predicates) {
        if (predicates.subjectContainsUrgent())
            return eval140710993279904(predicates);
        else
            return eval140710993280064(predicates);
    }

    private double eval140710993280064(EmailPredicates predicates) {
        if (predicates.subjectContainsDeadline())
            return eval140710993280032(predicates);
        else
            return eval140710993278144(predicates);
    }

    private double eval140710993280032(EmailPredicates predicates) {
        if (predicates.subjectContainsOverdue())
            return eval140710993279904(predicates);
        else
            return eval140710993280000(predicates);
    }

    private double eval140710993280000(EmailPredicates predicates) {
        if (predicates.subjectContainsNewsletter())
            return eval140710993279744(predicates);
        else
            return eval140710993279968(predicates);
    }

    private double eval140710993279968(EmailPredicates predicates) {
        if (predicates.fromGov())
            return eval140710993279712(predicates);
        else
            return eval140710993279936(predicates);
    }

    private double eval140710993279936(EmailPredicates predicates) {
        if (predicates.inAddressBook())
            return eval140710993279712(predicates);
        else
            return eval140710993278144(predicates);
    }

    private double eval140710993279712(EmailPredicates predicates) {
        if (predicates.fromShopA())
            return eval140710993278144(predicates);
        else
            return eval140710993279680(predicates);
    }

    private double eval140710993279680(EmailPredicates predicates) {
        if (predicates.fromShopB())
            return eval140710993278144(predicates);
        else
            return eval140710993279648(predicates);
    }

    private double eval140710993279744(EmailPredicates predicates) {
        if (predicates.fromGov())
            return eval140710993279712(predicates);
        else
            return eval140710993278144(predicates);
    }

    private double eval140710993279904(EmailPredicates predicates) {
        if (predicates.subjectContainsNewsletter())
            return eval140710993279744(predicates);
        else
            return eval140710993279872(predicates);
    }

    private double eval140710993279872(EmailPredicates predicates) {
        if (predicates.fromGov())
            return eval140710993279808(predicates);
        else
            return eval140710993279840(predicates);
    }

    private double eval140710993279840(EmailPredicates predicates) {
        if (predicates.inAddressBook())
            return eval140710993279808(predicates);
        else
            return eval140710993278144(predicates);
    }

    private double eval140710993279808(EmailPredicates predicates) {
        if (predicates.fromShopA())
            return eval140710993278144(predicates);
        else
            return eval140710993279776(predicates);
    }

    private double eval140710993279776(EmailPredicates predicates) {
        if (predicates.fromShopB())
            return eval140710993278144(predicates);
        else
            return eval140710993277152(predicates);
    }

    private double eval140710993278144(EmailPredicates predicates) {
        return 0.0;
    }

    private double eval140710993279648(EmailPredicates predicates) {
        return 0.5;
    }

    private double eval140710993277152(EmailPredicates predicates) {
        return 1.0;
    }
}
